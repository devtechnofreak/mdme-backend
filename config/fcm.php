<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAA05W4EYY:APA91bEhVH0POu1xGAbFr6WYygAvQpF2zQ9KenkCNBxRC3LclpB-essrJ3DGXsmYhgzrg_13HiOZ-KHHTFGDI9S7mOC2EuQOSeb-Z2yQcp9BcJsjrPcib2bf-duMD56dxxE0uSMdNEt-'),
        'sender_id' => env('FCM_SENDER_ID', '908749967750'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
