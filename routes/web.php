<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//Auth::routes();

Route::post('login', 'AuthController@login');

Route::post('register', 'AuthController@register');

Route::post('updateProfile', 'AuthController@updateProfile');

Route::get('getAllUsers', 'AuthController@getAllUsers');

Route::post('changePassword', 'AuthController@changePassword');

Route::post('forgotPassword', 'AuthController@forgotPassword');

Route::post('punchin', 'AuthController@punchin');

Route::post('punchout', 'AuthController@punchout');

Route::post('updatedriverlocation', 'AuthController@updateDriverLocation');

Route::post('getschedule','AuthController@getschedule');

Route::post('getservertime','AuthController@getservertime');

Route::post('getdispatchertime','AuthController@getdispatchertime');

Route::post('updatetripstatus','AuthController@updatetripstatus');

Route::post('sendmessage','AuthController@sendmessage');

Route::post('getmessage','AuthController@getmessage');

Route::post('gethistory','AuthController@gethistory');

Route::post('refreshFcm','AuthController@refreshFcm');
Route::get('test','AuthController@test');
Route::post('testBySiddhesh','AuthController@testBySiddhesh');

Route::group(['middleware' => 'cors'], function() {
	
Route::post('sendMessageNotification','AuthController@sendMessageNotification');

Route::post('getTripUpdate','AuthController@getTripUpdate');

Route::post('sendTripNotification','AuthController@sendBothWayTripNotification');

});




