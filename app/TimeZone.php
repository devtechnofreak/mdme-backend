<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class TimeZone extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'TimeZone';
	public $timestamps = false;
	protected $primaryKey ='Id';
	
    protected $fillable = array(
    
        'UserId','TimeZone','Isdelete'
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
    */
    protected $hidden = [
    ];
	

}