<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'IMHistory';
	public $timestamps = false;
	protected $primaryKey ='ChatId';
	
    protected $fillable = array(
        'FromUserName','ToUserName','Message','IsRead','MessageTime'
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
    */
    protected $hidden = [
    ];
	

}