<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        //AuthorizationException::class,
        //HttpException::class,
        //ModelNotFoundException::class,
        //ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
      //return parent::render($request, $e);
      	$success = false;
		if ($e instanceof ValidationException) {
          $response = $e->getResponse();
		  //dd($response);
			return response()->json([
	          'status' => $success,
	          'status_code' => 401,
	          'message' => array_values(json_decode($response->getContent(),true))[0][0]
	        ]);
        }
		if($e instanceof UnauthorizedHttpException) {
			$status = 403;
		}else{
			$status = 401;
		}
    	return response()->json([
    	  'status' => $success,
          'status_code' => $status,
          'message' => $e->getMessage()
        ]);
  /*      $success = false;
        $response = null;
        $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        if ($e instanceof HttpResponseException) {
          $status = Response::HTTP_INTERNAL_SERVER_ERROR;
          $response = $e->getResponse();
        } elseif ($e instanceof MethodNotAllowedHttpException) {
          $status = Response::HTTP_METHOD_NOT_ALLOWED;
          $e = new MethodNotAllowedHttpException([], 'HTTP_METHOD_NOT_ALLOWED', $e);
        } elseif ($e instanceof NotFoundHttpException) {
          $status = Response::HTTP_NOT_FOUND;
          $e = new NotFoundHttpException('HTTP_NOT_FOUND', $e);
        } elseif ($e instanceof AuthorizationException) {
          $status = Response::HTTP_FORBIDDEN;
          $e = new AuthorizationException('HTTP_FORBIDDEN', $status);
        } elseif ($e instanceof ValidationException) {
          $response = $e->getResponse();
		  //dd($response);
			return response()->json([
	          'status' => $success,
	          'status_code' => 401,
	          'message' => array_values(json_decode($response->getContent(),true))[0][0]
	        ]);
        }elseif ($e) {
          $e = new HttpException($status, 'HTTP_INTERNAL_SERVER_ERROR');
        }
        return response()->json([
          'status' => $success,
          'status_code' => $status,
          'message' => $e->getMessage()
        ]);
   * 
   */
    }
}
