<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\File;
use App\Dispatcher;
use App\DriverLocation;
use App\OdometerReading;
use Illuminate\Support\Facades\File as Filef;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Trip;
use App\DriverHistory;
use App\Signature;
use App\Message;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class AuthController extends Controller
{
	public function __construct()
	{
	    $this->middleware('auth:api', ['except' => ['login','register','getAllUsers','forgotPassword','getservertime','sendMessageNotification','getTripUpdate','sendBothWayTripNotification']]);
	}

   public function login(Request $request)
   {
		$this->validate($request, array(
            'username'     => 'required|max:255',
            'password' 	   => 'required',
            'device_token' => 'required',
            'app_version'  => 'required',
            'device_name'  => 'required',
   			));
		$user = User::where('Username',$request->username)->where('Password',$request->password)->first();
		
		if($user)
		{
			$user->deviceinfo()->updateOrCreate(['Driver_ID'=>$user->ID],[
													'Device_Token'=> $request->device_token,
													'Device_Type'=>$request->header('OS'),
													'App_Version'=>$request->app_version,
													'Device_Name'=>$request->device_name,
													'Date_Created'=>date('Y-m-d H:i:s',time()),
													'Access_Token'=>$this->generateRandomString()
												]);
			$userData = User::find($user->ID);
			$data = array(
					'status_code' => 200,
					'message'=>"login successfully",
					'data'=>array('user_data'=>array($userData),'veh_data'=>$userData->vehiclelist()),
					'server_date'=>date('Y-m-d H:i:s',time()),
					'server_timezone'=>"PDT"
					);
			return response()->json($data);	
		} else {
			$data = array(
					'status_code' => 401,
					'message'=>"Credentials are wrong please try again",
					'data'=>array('user_data'=>array(),'veh_data'=>array()),
					'server_date'=>date('Y-m-d H:i:s',time()),
					'server_timezone'=>"PDT"
					);
			return response()->json($data);	
		}
   }
   
   public function register(Request $request)
   {
		$this->validate($request, array(
            'username'    => 'required|max:255|unique:Drivers,Username',
			'FirstName'	  =>'required',
			'LastName'	  =>'required',
			'DOB'	  	  =>'required',
			'EmailAddress'=>'required|email',
			'DCode'	  	  =>'required',
			'password'	  =>'required|min:4',
			'PhoneNumber' =>'required|min:10',
			'HireDate'    =>'required',
			'Address'     =>'required',
			'device_token'=>'required',
            'app_version' =>'required',
            'device_name' =>'required',
            'OwnerUsername'=>'required|exists:Users,Username'
   			));
		$fileObject = false;
		if($request->hasFile('Picture')) {
            $file = $request->file('Picture');
            $name = strtotime("now").str_replace(' ', '_', $file->getClientOriginalName());
			$size = Filef::size($file);
			$path = getcwd()."\..\..\dmelive.com\Images";
			$mime = Filef::mimeType($file);
			$unpacked = unpack("H*hex", pack("L", $size));
            $file->move($path,$name);
			
			try{
				$fileInsert = DB::statement("INSERT INTO Files ([fkUsername],[FileName],[FileSize],[ContentType],[FileData]) 
												VALUES ('".$request->OwnerUsername."',
												'".$name."',
												$size,
												'".$mime."',
												0x".$unpacked['hex'].")");
				$fileObject = File::find(DB::getPdo()->lastInsertId());
			} catch(Exception $e){
				$result['status_code'] = 401;
	            $result['message'] = "Failed to upload Image.";
				return response()->json($result);
			}        }
		try{
			User::firstOrCreate(['Username'=>$request->username],[
	    				'FirstName'=>$request->FirstName,
	    				'LastName'=>$request->LastName,
	    				'DOB'=> date('Y-m-d',strtotime($request->DOB)),
	    				'Password'=>$request->password,
	    				'PhoneNum'=>(string)$request->PhoneNumber,
	    				'AlternateNum'=>(string)$request->input('AltNumber'),
	    				'Address'=>$request->Address,
	    				'EmailAddress'=>$request->EmailAddress,
	    				'Active'=>1,
	    				'MI'=>'',
	    				'HireDate'=>date('Y-m-d',strtotime($request->HireDate)),
	    				'DCode'=>$request->DCode,
	    				'fkUsername'=>$request->OwnerUsername,
	    				'fkPictureFileID'=>$fileObject?$fileObject->FileID:0		
			]);
			$result['status_code'] = 200;
            $result['message'] = "Registered Successfully, Login now.";
			return response()->json($result);
		} catch(Exception $e){
			$result['status_code'] = 401;
            $result['message'] = "Something went wrong, try again later.";
			return response()->json($result);
		}
   } 

   public function updateProfile(Request $request)
   {
   		$this->validate($request, array(
            'DriverId'    => 'required|integer',
			'FirstName'	  =>'required',
			'LastName'	  =>'required',
			'DOB'	  	  =>'required',
			'EmailAddress'=>'required|email',
			'DCode'	  	  =>'required',
			'PhoneNumber' =>'required|min:10',
			'Address'     =>'required',
            'OwnerUsername'=>'required|exists:Users,Username'
   			));
			
		$fileObject = false;
		if($request->hasFile('Picture')) {
            $file = $request->file('Picture');
            $name = strtotime("now").str_replace(' ', '_', $file->getClientOriginalName());
			$size = Filef::size($file);
			$path = getcwd()."\..\..\dmelive.com\Images";
			$mime = Filef::mimeType($file);
			$unpacked = unpack("H*hex", pack("L", $size));
            $file->move($path,$name);
			
			try{
				$fileInsert = DB::statement("INSERT INTO Files ([fkUsername],[FileName],[FileSize],[ContentType],[FileData]) 
												VALUES ('".$request->OwnerUsername."',
												'".$name."',
												$size,
												'".$mime."',
												0x".$unpacked['hex'].")");
				$fileObject = File::find(DB::getPdo()->lastInsertId());
			} catch(Exception $e){
				$result['status_code'] = 401;
	            $result['message'] = "Failed to upload Image.";
				return response()->json($result);
			}
        }
		$user = User::find($request->DriverId);
		try{
			$user->update([
	    				'FirstName'=>$request->FirstName,
	    				'LastName'=>$request->LastName,
	    				'DOB'=> date('Y-m-d',strtotime($request->DOB)),
	    				'PhoneNum'=>(string)$request->PhoneNumber,
	    				'AlternateNum'=>(string)$request->input('AltNumber'),
	    				'Address'=>$request->Address,
	    				'EmailAddress'=>$request->EmailAddress,
	    				'DCode'=>$request->DCode,
	    				'fkPictureFileID'=>$fileObject?$fileObject->FileID:$user->fkPictureFileID
				]);
			$result['status_code'] = 200;
	        $result['message'] = "Profile has been updated Successfully";
			$result['user_data']=$user;
		} catch(Exception $e){
			$result['status_code'] = 401;
            $result['message'] = "Something went wrong, Try again later.";
		}
		return response()->json($result);
   }
   
   public function getAllUsers()
   {
	   	try{
	   		$dispatcher  = Dispatcher::where('Active',1)->whereRaw('ISNULL(Demo,0)=0')->get();
			$result['status_code'] = 200;
			$result['data']  = $dispatcher;
		} catch(Exception $e){
			$result['status_code'] = 401;
            $result['message'] = "Something went wrong, Try again later.";							
		}
		
		return response()->json($result);
   }
   
   public function changePassword(Request $request)
   {
   		$this->validate($request, array(
            'DriverId'    => 'required|integer',
			'OldPassword' =>'required',
			'NewPassword' =>'required|min:4',
   			));
		$user = User::find($request->DriverId);
		if($user && $user->Password == $request->OldPassword)
	    {
	    	try{
	    		$user->Password = $request->NewPassword;
				$user->save();
				$result['status_code'] = 200;
            	$result['message'] = "Password has been changed successfully.";
	    	} catch(Exception $e){
				$result['status_code'] = 500;
	            $result['message'] = "Failed to update password..";							
			}
	    } else {
	    	$result['status_code'] = 401;
            $result['message'] = "Password Mismatch.";
	    }
		
		return response()->json($result);
   }
   
   public function forgotPassword(Request $request)
   {
   		$this->validate($request,array(
   			'username'=>'required|exists:Drivers,Username'
		));
		
		$user = User::where('Username',$request->username)->first();
		if($user): 
		$newPassword = $this->generateRandomPassword(8);
		$data['message'] = '<strong>Your Username:'.$user->UserName.'<br/><br/></strong>';
		$data['message'].= '<strong>Your New Password:'.$newPassword.'<br/><br/></strong>';
		$data['name']= ucfirst($user->FirstName.' '.$user->LastName);
		$data['mail']= $user->EmailAddress;
		try {
		 Mail::send([],[], function ($message) use($data){
		            $message->from('noreply@dmelive.com','DME');
		            $message->subject('New Password');
		            $message->to($data['mail'], $data['name']);
					$message->setBody($data['message'], 'text/html');
		 });
		 $user->Password = $newPassword;
		 $user->save();
		 return response()->json(['message'=>'Your new password has been sent to your registered email address.','status_code'=>200]);
		 } catch (Exception $e) {
		 	return response()->json(['message' => 'Unable to send mail.','status_code'=>401]);
		 }	
		else:
			return response()->json(['message' => 'Username Invalid.','status_code'=>401]);
		endif;
   	
   }

   public function punchin(Request $request)
   {
   		$this->validate($request,array(
   			'driver_id'=>'required|integer',
   			'vehicle_id'=> 'required|integer',
			'odometer_reading'=> 'required|integer',
			'timezone'=> 'required',
			'fkusername'=> 'required|exists:Users,Username'
		));  
		try {
			$odoReading = OdometerReading::create([
							"fkVehicleID" 		=> $request->vehicle_id,
                            'fkDriverID'      	=> $request->driver_id,
                            'fkUserName'      	=> $request->fkusername,
                            'TripDate'   		=> date('m-d-Y',time()),
                            'StartTime'			=> date('g:iA',time()),
                            'StartReading'		=> $request->odometer_reading,
                            'Date'				=> date('Y-m-d H:i:s', time()),
                            'Timezone'			=> $request->timezone
							]); 
			$res['data']['punch_id']= $odoReading->ID;
			$result['status_code'] = 200;
            $result['message'] = "Punched IN Successfully";
			$result['server_date']=date('Y-m-d H:i:s', time());
			$result['server_timezone']=date('T');
			$result['data']  = $odoReading;
		} catch(Exception $e){
			$result['status_code'] = 500;
            $result['message'] = "Punched IN failed";
            $result['data']  = array(); 
		}
		
		return response()->json($result);
	
   }

   public function punchout(Request $request)
   {
   		$this->validate($request,array(
   			'driver_id'=>'required|integer',
   			'punch_id'=> 'required|integer',
			'odometer_reading'=> 'required|integer',
		));  
		try {
			$odoReading = OdometerReading::find($request->punch_id);
			$odoReading->update([
							'EndTime' 			=> date('g:iA'),
                            'EndReading'      	=> $request->odometer_reading,
                            'enddate'			=> date('Y-m-d H:i:s',time())
							]); 
			
			$deviceInfo = Auth::user()->deviceinfo;
			$deviceInfo->Access_Token = '';
			$deviceInfo->save();
			$res['data']['punch_id']= $odoReading->ID;
			$result['status_code'] = 200;
            $result['message'] = "Punched OUT Successfully";
			$result['server_date']=date('Y-m-d H:i:s', time());
			$result['server_timezone']=date('T');
			$result['data']  = $odoReading;
		} catch(Exception $e){
			$result['status_code'] = 500;
            $result['message'] = "Punched OUT failed";
            $result['data']  = array(); 
		}   	
		
		return response()->json($result);
   }
   
   public function updateDriverLocation(Request $request)
   {
   		$this->validate($request,array(
   			'driver_id'=>'required|integer',
			'latitude'=> 'required',
			'longitude'=> 'required'
		)); 
		try {
			$driverLocation = DriverLocation::updateOrCreate(['DriverID'=>$request->driver_id],[
									'Lat'	  =>$request->latitude,
									'Long'    =>$request->longitude,
									'Datetime'=>date('Y-m-d H:i:s', time())
								]);  
			$result['status_code'] = 200;
            $result['message'] = "Location updated successfully";
			$result['server_date']=date('Y-m-d H:i:s', time());
			$result['server_timezone']=date('T');
		} catch(Exception $e) {
			$result['status_code'] = 500;
            $result['message'] = "Location update failed";
		}
		
		return response()->json($result);	
   }	

   public function getschedule(Request $request)
   {
   		$this->validate($request,array(
   			'driver_id'=>'required|integer',
			'schedule_date'=> 'required',
		));    
		$trips = DB::table('Trips')
				 ->join('Customers', 'Trips.fkCustID', '=', 'Customers.ID')
				 ->select('Trips.*', DB::raw("(Customers.LastName+', '+Customers.FirstName) as customer_name"))
				 ->whereRaw('CAST(Trips.Time AS DATE) = ?',date('Y-m-d', strtotime($request->schedule_date)))
				 ->where(function ($query) use($request) {
		                $query->where('Trips.fkDrvrTo', $request->driver_id)
		                      ->orWhere('Trips.fkDrvrFrom', $request->driver_id);
	             })->orderBy('Trips.Time', 'desc')
				 ->get();
				 
		$single_trip = $return_trip = $data = array();
		$trips = collect($trips)->map(function($x){ return (array) $x; })->toArray();
		foreach ($trips as $key =>$row) {
			if($row['Time'])$trips[$key]['Time']= date('Y-m-d H:i:s', strtotime($row['Time']));
			if(date('Y-m-d',strtotime($request->schedule_date)) <= date('Y-m-d', time()))
			{
			if(is_null($row['OStatus']))$trips[$key]['OStatus']='N';
			if(is_null($row['Rstatus']))$trips[$key]['Rstatus']='N';
			}
			$from = $this->getLatLong($row['From']);
			$to = $this->getLatLong($row['To']);
			$trips[$key]['from_lat'] = $from['latitude']?$from['latitude']:'Not found';
			$trips[$key]['from_long'] = $from['longitude']?$from['longitude']:'Not found';
			$trips[$key]['to_lat'] = $to['latitude']?$to['latitude']:'Not found';
			$trips[$key]['to_long'] = $to['longitude']?$to['longitude']:'Not found';
			if($row['OStatus']!='C' && $row['fkDrvrTo'] == $request->driver_id) $single_trip[$key] = $trips[$key];
			if($row['Ret']==1 && $row['Rstatus']!='R' && $row['fkDrvrFrom'] == $request->driver_id) $return_trip[$key] = $trips[$key];
		}
		if(count($trips) >0)
		{
			$data['single_trip']  = array_values($single_trip);
			$data['return_trip']  = array_values($return_trip);
			$result['status_code'] = 200;
            $result['message'] = "Trip listed Successfully";
			$result['server_date']=date('Y-m-d H:i:s', time());
			$result['server_timezone']=date('T');
			$result['data']  = $data; 
		} else {
			
			$data['single_trip']  = array();
			$data['return_trip']  = array();
			$result['status_code'] = 500;
            $result['message'] = "No Trip is Currently Scheduled";
            $result['data']  = $data; 
      	}
		return response()->json($result);
   }

   public function updatetripstatus(Request $request)
   {
   		$this->validate($request,array(
   			'driver_id'=>'required|integer',
			'trip_id'=> 'required|integer',
			'trip_status'=> 'required',
			'trip_type'=> 'required',
		));
		
   		$trip = Trip::find($request->trip_id);
		if(!$trip || ($trip->fkDrvrTo != Auth::user()->ID || $trip->fkDrvrFrom != Auth::user()->ID)){
			$result['status_code'] = 500;
            $result['message'] = "Can't update trip status";
			return response()->json($result);
		}
		
		if(Auth::user()->dispatcher->timezone->TimeZone)
		$date = new \DateTime(null, new \DateTimeZone(timezone_name_from_abbr(Auth::user()->dispatcher->timezone->TimeZone)));
		else
		$date = new \DateTime();   
		
		$update_Trip = array(
							"status" =>$request->trip_status,
							"OStatus" =>$request->trip_type == 'single'?'N':'C',
							"Rstatus" =>'N',
							"ReturningNow"=>0,
							"ToTime"=>$date->format('Y-m-d H:i:s')
							);
		
		$complete_Singletrip = array(
									"status" =>$request->trip_status,
									"OStatus" =>'C',
									"Rstatus" =>'N',
									"ReturningNow"=>$trip->Ret == 1?0:1,
									"ToTime"=>$date->format('Y-m-d H:i:s')
									);
		
		$complete_Returntrip = array(
									"status" =>$request->trip_status,
									"ReturningNow"=>1,
									"OStatus" =>'C',
									"Rstatus" =>'R',
									"ToTime"=>$date->format('Y-m-d H:i:s')
									);
									
		switch($request->trip_status)
		{
			case '1':
				$trip->update($update_Trip);
				break;
				
			case '2':
				$trip->update($update_Trip);
				break;
				
			case '3':
				$trip->update($complete_Singletrip);
				break;
				
			case '5':
				if($request->trip_type == 'single'){
					$trip->update($complete_Singletrip);
				} else {
					$trip->update($complete_Returntrip);
				}
				break;
		}	
		if($request->input('sign_base64')){
			Signature::create([
							'TripId'=>$request->trip_id,
							'Coordinates'=>'',
							'SignatureImgPath' => $request->input('sign_base64'),
							'IsPickup'=>$request->trip_status==1?1:0,
							'createdDate'=>date('Y-m-d H:i:s',time())
							]);
		}
		
		$result['status_code'] = 200;
        $result['message'] = "Trip status updated Successfully";
		$result['server_date']=date('Y-m-d H:i:s', time());
		$result['server_timezone']=date('T');
		return response()->json($result);
   }
   
   public function getservertime(Request $request)
   {
   		$result['status_code'] = 200;
		$result['server_date']=date('Y-m-d H:i:s', time());
		$result['server_timezone']=date('T');
		$result['message'] = "server time sent successfully";
		return response()->json($result);
   }
   
   public function sendmessage(Request $request)
   {
   		$this->validate($request,array(
   			'driver_id'=>'required|integer',
			'message'=> 'required',
			'dispatcherName'=> 'required',
			'driverName'=> 'required',
		));  
		try {
			Message::create([
                        "FromUserName" 	=> $request->driverName,
                        'ToUserName'    => $request->dispatcherName,
                        'Message'      	=> $request->message,
                        'IsRead'   		=> 0,
                        'MessageTime'	=> date('Y-m-d H:i:s', time())
						]);
			$result['status_code'] = 200;
            $result['message'] = "Message sent successfully";
			$result['server_date']=date('Y-m-d H:i:s', time());
			$result['server_timezone']=date('T');
		} catch(Exception $e) {
			$result['status_code'] = 500;
            $result['message'] = "Unable to send message";
		}

		return response()->json($result);
		 	
   }
   
   public function getmessage(Request $request)
   {
   		$this->validate($request,array(
   			'driver_id'=>'required|integer',
			'dispatcherName'=> 'required',
			'driverName'=> 'required',
		));  
		$messages = Message::where('FromUserName',$request->dispatcherName)
							->where('ToUserName',$request->driverName)
							->where('IsRead',0)
							->get();
		
		if(count($messages)>0){
			
		Message::where('FromUserName',$request->dispatcherName)->where('ToUserName',$request->driverName)->update(['IsRead' => 1]);
		
		$result['status_code'] = 200;
        $result['message'] = "Message sent successfully";
		$result['server_date']=date('Y-m-d H:i:s', time());
		$result['server_timezone']=date('T');
		$result['data'] = $messages;
		
		} else {
			$result['status_code'] = 500;
            $result['message'] = "No more unread message";
			$result['data']=array();
		} 
		return response()->json($result);	
   }
   
   public function gethistory(Request $request)
   {
   		$this->validate($request,array(
   			'driver_id'=>'required|integer',
			'dispatcherName'=> 'required',
			'driverName'=> 'required',
			'offset'=>'required|integer'
		));
		try {
			if($request->offset >0)$cond ="AND ChatId < ".$request->offset;  
			else  $cond ="AND 1=1"; 
			$messages = DB::select("DECLARE @PageSize INT,
									        @Page INT
									
									SELECT  @PageSize = 20,
									        @Page = 1
									
									;WITH PageNumbers AS(
									        SELECT *,
									                ROW_NUMBER() OVER(ORDER BY ChatId DESC) ID
									        FROM    IMHistory
									        WHERE ((FromUserName='".$request->dispatcherName."' AND ToUserName ='".$request->driverName."') 
		                            		OR (FromUserName='".$request->driverName."' AND ToUserName ='".$request->dispatcherName."'))
		                            		".$cond."
									)
									SELECT  *
									FROM    PageNumbers
									WHERE   ID  BETWEEN ((@Page - 1) * @PageSize + 1)
									        AND (@Page * @PageSize)
									");
							 
			Message::where('FromUserName',$request->dispatcherName)->where('ToUserName',$request->driverName)->update(['IsRead' => 1]);
			$result['status_code'] = 200;
            $result['message'] = "chat history listed successfully";
			$result['server_date']=date('Y-m-d H:i:s', time());
			$result['server_timezone']=date('T');
			$result['data']=array_reverse($messages);
		} catch(Exception $e) {
			
			$result['status_code'] = 500;
            $result['message'] = "No more unread message";
			$result['data']=array();
		}

		return response()->json($result);
   }
	public function refreshFcm(Request $request){
        $this->validate($request, [
            'device_token' => 'required',
        ]);	
        try {	
		$user = User::find(Auth::user()->ID);
		//if(isset($user->DeviceToken))
		$user->deviceinfo->Device_Token = $request->device_token;
		$user->deviceinfo->save();
		$data = array('message' => 'FCM has been updated successfully.','status_code'=>200);
	    } catch(Exception $e) {
			$data = array('message' => 'Refresh fcm failed.','status_code'=>401);
		}
		return response()->json($data);
	}


	public function sendMessageNotification(Request $request)
	{
		$this->validate($request,array(
			'chat_id'=> 'required|integer'
		));
		$chat = Message::find($request->chat_id);
		$chat = DB::table('IMHistory')
				->join('Drivers', 'IMHistory.ToUserName', '=', 'Drivers.Username')
				->join('Device_Info', 'Drivers.ID', '=', 'Device_Info.Driver_ID')
				->select('IMHistory.*','Device_Info.Device_Token','Device_Info.Device_Type')
				->where('IMHistory.ChatId',$request->chat_id)
				->first();
				
		if($chat):
		$message = array(
						'title'			=> 'DME LIVE',
						'ChatId'		=> $request->chat_id,
						'created_at'	=> date('Y-m-d h:i:s'),
						'is_background'	=> true,
						'message'		=> $chat->Message,
						'ToUserName'    => $chat->ToUserName,
                    	'FromUserName'	=> $chat->FromUserName,
						'timestamp'		=> date('Y-m-d h:i:s'),
						'payload'		=> array(),
						'click_action'	=> 'dme.activity.chat'
						);
		$this->sendFcmPush($chat->Device_Token,'chat',$message,$chat->Device_Type);
		endif;
		$result['status_code'] = 200;
        $result['message'] = "notification sent successfully";
		return response()->json($result);
	}

	public function getTripUpdate(Request $request)
	{
		$this->validate($request,array(
			'trip_id'=> 'required|integer'
		));
		
		 $new_driver = Trip::find($request->trip_id);
		 $old_driver = DriverHistory::where('TripID',$request->trip_id)->orderBy('ID','DESC')->first();
		 if($new_driver && $old_driver){
		 	if($old_driver->DrvrTo != $new_driver->fkDrvrTo && !empty($new_driver->fkDrvrTo) && !empty($old_driver->DrvrTo)){
		 		$status=2;
				$trip_type="single";
				$this->sendTripNotification($new_driver,$old_driver->DrvrTo,$status,$trip_type);
				$status=1;
				$this->sendTripNotification($new_driver,$new_driver->fkDrvrTo,$status,$trip_type);
		 	}
			
		 	if($old_driver->Drvrfrom != $new_driver->fkDrvrfrom && !empty($new_driver->fkDrvrfrom) && !empty($old_driver->Drvrfrom)){
		 		$status=2;
				$trip_type="return";
				$this->sendTripNotification($new_driver,$old_driver->Drvrfrom,$status,$trip_type);
				$status=1;
				$this->sendTripNotification($new_driver,$new_driver->fkDrvrfrom,$status,$trip_type);
		 	}
		 	
		 }
		 $result['status_code'] = 200;
         $result['message'] = "notification sent successfully";
		 return response()->json($result);
	}

	public function sendBothWayTripNotification(Request $request)
	{
		$this->validate($request,array(
			'trip_id'=> 'required|integer'
		));	
		
		$new_driver = Trip::find($request->trip_id);
	    $old_driver = DriverHistory::where('TripID',$request->trip_id)->orderBy('ID','DESC')->first();	
		if(!empty($new_driver)){
			if(!empty($old_driver)){
				If($old_driver->DrvrTo != $new_driver->fkDrvrTo) { //if old driverout and new driverout is different
					$DriverOutChange = true;
				} else {
					$DriverOutChange = false;
				} 
				
				If($old_driver->Drvrfrom != $new_driver->fkDrvrfrom) { //if old driverRet and new driverRet is different
					$DriverRetChange = true;
				} else {
					$DriverRetChange = false;
				}
				
				if($DriverOutChange && $DriverRetChange) {
					if($old_driver->Drvrfrom == $old_driver->DrvrTo) { //if Old driverOut and Old driverRet is same
						$trip_type='both';
						$status=2;
						if(!empty($old_driver->Drvrfrom)) {     //if DriverRetOld is not null or empty
							$this->sendTripNotification($new_driver,$old_driver->Drvrfrom,$status,$trip_type);  //send pushnotification for cancel pickup&return trip to DriverRet old or DriverOut old
						}
					} else {                        //if Old driverOut and Old driverRet are different
						$trip_type='single';
						$status=2;
						if(!empty($old_driver->DrvrTo)) {    //if DriverOutOld is not null or empty
							$this->sendTripNotification($new_driver,$old_driver->DrvrTo,$status,$trip_type); //send pushnotification for cancel pickup trip to DriverOut old
						}
						$trip_type='return';
						$status=2;
						if(!empty($old_driver->Drvrfrom)) {    //if DriverRetOld is not null or empty
							$this->sendTripNotification($new_driver,$old_driver->Drvrfrom,$status,$trip_type);  //send pushnotification for cancel return trip to DriverRet old
						}
					}
				
					if($new_driver->fkDrvrfrom == $new_driver->fkDrvrTo) {  //if New driverOut and New driverRet is same
						$trip_type='both';
						$status=1;
						if(!empty($new_driver->fkDrvrfrom)) {     //if DriverRetOld is not null or empty
							$this->sendTripNotification($new_driver,$new_driver->fkDrvrfrom,$status,$trip_type);  //send pushnotification for cancel pickup&return trip to DriverRet old or DriverOut old
						}
					} else {
						$trip_type='single';
						$status=1;
						if(!empty($new_driver->fkDrvrTo)) {
							$this->sendTripNotification($new_driver,$new_driver->fkDrvrTo,$status,$trip_type); // send pushnotification for schedule pickup trip to driverOut new
						}
						$trip_type='return';
						$status=1;
						if(!empty($new_driver->fkDrvrfrom)) {
							$this->sendTripNotification($new_driver,$new_driver->fkDrvrfrom,$status,$trip_type);  // send pushnotification for schedule return trip to driverRet new
						}
					}
				} else if($DriverOutChange){
					$trip_type='single';
					$status=2;
					if(!empty($old_driver->DrvrTo)) {    //if DriverOutOld is not null or empty
						$this->sendTripNotification($new_driver,$old_driver->DrvrTo,$status,$trip_type); //send pushnotification for cancel pickup trip to DriverOut old
					}
					$trip_type='single';
					$status=1;
					if(!empty($new_driver->fkDrvrTo)) {
						$this->sendTripNotification($new_driver,$new_driver->fkDrvrTo,$status,$trip_type); // send pushnotification for schedule pickup trip to driverOut new
					}
				} else if($DriverRetChange){
					$trip_type='return';
					$status=2;
					if(!empty($old_driver->Drvrfrom)) {    //if DriverRetOld is not null or empty
						$this->sendTripNotification($new_driver,$old_driver->Drvrfrom,$status,$trip_type);  //send pushnotification for cancel return trip to DriverRet old
					}
					$trip_type='return';
					$status=1;
					if(!empty($new_driver->fkDrvrfrom)) {
						$this->sendTripNotification($new_driver,$new_driver->fkDrvrfrom,$status,$trip_type);  // send pushnotification for schedule return trip to driverRet new
					}					
				} else {
					
				}
				
			} else {
				$trip_type='single';
				$status=1;
				if(!empty($new_driver->fkDrvrTo)) {
					$this->sendTripNotification($new_driver,$new_driver->fkDrvrTo,$status,$trip_type); // send pushnotification for schedule pickup trip to driverOut new
				}
				$trip_type='return';
				$status=1;
				if(!empty($new_driver->fkDrvrfrom)) {
					$this->sendTripNotification($new_driver,$new_driver->fkDrvrfrom,$status,$trip_type);  // send pushnotification for schedule return trip to driverRet new
				}
			}
		}
		$result['status_code'] = 200;
        $result['message'] = "notification sent successfully";
		return response()->json($result);
	}

	protected function sendTripNotification($trip,$driverId,$status,$tripType)
	{
		$user = User::find($driverId);
		
		if($tripType =="single") $flag="pickup trip";
		if($tripType =="return") $flag="return trip";
		if($tripType =="both") $flag="pickup & return trip";
		
		if($status == 2){
			$noti = 'Your '.$flag.' on '.date("Y-m-d",strtotime($trip->Time)). ' at '.date("h:i A",strtotime($trip->Time)). ' for customer '.$trip->customer->LastName.' '.$trip->customer->FirstName.' has been canceled.';
			$trip_status ="cancel";
		} else {
			$noti = 'Your '.$flag.' on '.date("Y-m-d",strtotime($trip->Time)). ' at '.date("h:i A",strtotime($trip->Time)). ' for customer '.$trip->customer->LastName.' '.$trip->customer->FirstName.' has been scheduled.';
			$trip_status ="new";
		}
		if($user->deviceinfo->Device_Token):
		$message = array(
						'title'			=> 'DME LIVE',
						'created_at'	=> date('Y-m-d h:i:s'),
						'is_background'	=> true,
						'message'		=> $noti,
						'status'		=> $trip_status,
						'data'			=> json_encode($trip),
						'flag'			=> $flag,
						'timestamp'		=> date('Y-m-d h:i:s'),
						'payload'		=> array(),
						'click_action'	=> 'dme.activity.trip'
						);
		$this->sendFcmPush($user->deviceinfo->Device_Token,'trip',$message,$user->deviceinfo->Device_Type);
		endif;
		return true;
	}

	protected function sendFcmPush($token,$action,$message,$os=null){
		$optionBuilder = new OptionsBuilder(); 
		$optionBuilder->setTimeToLive(60*20);
		$optionBuilder->setPriority('high');
		$optionBuilder->setContentAvailable(true);
		
		$notificationBuilder = new PayloadNotificationBuilder('DME LIVE');
		$notificationBuilder->setBody($message['message'])
							->setClickAction('dme.activity.'.$action)
						    ->setSound('default');
		
		$dataBuilder = new PayloadDataBuilder();
		$dataPayload = array('data'=> $message);
		$dataBuilder->addData($dataPayload);
		
		$option = $optionBuilder->build();
		$notification = $notificationBuilder->build();
		
		if($os == 'android')
		$notification=null;
		$data = $dataBuilder->build();	
		$downstreamResponse = FCM::sendTo($token, $option, $notification, $data);	
		//echo json_encode($downstreamResponse);
		
		return true;
	}
   
   protected function getLatLong($address)
   {
	    if(!empty($address)){
	        $url='https://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&key=AIzaSyB-8daOD7gH-ezUQ8_pX32EnGg3Ga59mjY&sensor=false'; 
	        $ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$responseJson = curl_exec($ch);
			if($responseJson === false){
				return false;
			}
			curl_close($ch);
	        $output = json_decode($responseJson);
			
	        $data['latitude']  = $output->results[0]->geometry->location->lat; 
	        $data['longitude'] = $output->results[0]->geometry->location->lng;
			
	        if(!empty($data)){
	            return $data;
	        } else {
	            return false;
	        }
	    } else {
	        return false;   
	    }   	
   }
   
   protected function generateRandomString($length = 40) 
   {
        $alphabets = range('A','Z');
        $numbers = range('0','9');
        $additional_characters = array('_','.');
        $final_array = array_merge($alphabets,$numbers,$additional_characters);
             
        $password = '';
      
        while($length--) {
          $key = array_rand($final_array);
          $password .= $final_array[$key];
        }
      
        return $password;
   }
   
   protected function generateRandomPassword( $length = 8 ) {
	    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$";
	    $password = substr( str_shuffle( $chars ), 0, $length );
	    return $password;
   }
}
