<?php

namespace App\Http\Middleware;

use Closure;
use App\DeviceInfo;
use Illuminate\Contracts\Auth\Factory as Auth;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
    	if(!$request->header('TOKEN')){
    		return response()->json(['message'=>'Unauthorized','status_code'=>403]);
    	}
		
    	$deviceInfo = DeviceInfo::where('Access_Token',$request->header('TOKEN'))->first();
		
		if(!$deviceInfo){
    		return response()->json(['message'=>'Your Session has been expired. Please login again.','status_code'=>403]);
    	}
		
		$request->merge([ 'api_token' => $deviceInfo->user->Username ]);
		
		$authId = $this->auth->user()->ID;
		
    	if($request->input('driver_id')){
    		if($request->driver_id != $authId){
    			return response()->json(['message'=>'Unauthorized','status_code'=>403]);
    		}
    	}
		
    	if($request->input('DriverId')){
    		if($request->DriverId != $authId){
    			return response()->json(['message'=>'Unauthorized','status_code'=>403]);
    		}
    	}
		
        if ($this->auth->guard($guard)->guest()) {
            return response()->json(['message'=>'Unauthorized','status_code'=>403]);
        }

        return $next($request);
    }
}
