<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'Vehicles';
	public $timestamps = false;
	protected $primaryKey ='ID';
	//protected $with = array('notes'); 
	
    protected $fillable = array(
    
        'fkUsername','VIN','Registration','DME_ID','VehicleName'
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
    */
    protected $hidden = [
        'fkUsername','VIN','Registration','DME_ID'
    ];
	

}