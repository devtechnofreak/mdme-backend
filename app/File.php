<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'Files';
	public $timestamps = false;
	protected $primaryKey ='FileID';
	
    protected $fillable = array(
    
        'fkUsername','FileName','FileSize','ContentType','FileData'
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
    */
    protected $hidden = [
    'fkUsername','FileSize','ContentType','FileData'
    ];
	

}