<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'Trips';
	public $timestamps = false;
	protected $primaryKey ='ID';
	//protected $with = array('customer'); 
	
    protected $fillable = array(
    'fkUsername','Time','fkCustID','PersonCalling','PhoneNum','From','To','Ret','POT','Cash','CheckNum','Medicaid','MedicaidNum','ApprovedBy','ApprovedOn','Needs','AssistHome','AssistDestination','Mileage','Comments'
    ,'fkDrvrTo','fkDrvrFrom','fkDriverToVehicle','fkDriverFromVehicle','Status','BillingInfo','BillingName','BillingAddress','BillingPhoneNum','ReturningNow','IsPending','GridVehicleID','GridTime','Address','TripStatus'
    ,'ToTime','OStatus','Rstatus','ReimbursementID','NotesForDriver','AWRTime','Out_Mileage','CancelledTime','EstDriveTime','FrequencyID','CreatedDate','gridtimeTemp','timeTemp'
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
    */
    protected $hidden = [
    ];
	
	protected function customer()
	{
		return $this->belongsTo('App\Customer','fkCustID','ID');
	}
	
	protected function signatures()
	{
		return $this->hasOne('App\Signature','TripId','ID');
	}

}