<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class DriverLocation extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'Driver_Location';
	public $timestamps = false;
	protected $primaryKey ='ID';
	
    protected $fillable = array(
       'DriverID','Lat','Long','Datetime'
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
    */
    protected $hidden = [
    ];
	

}