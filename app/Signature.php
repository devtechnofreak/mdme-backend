<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Signature extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'Signature';
	public $timestamps = false;
	protected $primaryKey ='SignatureId';
	
    protected $fillable = array(
    
        'SignatureId','TripId','Coordinates','createdDate','SignatureImgPath','IsPickup'
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
    */
    protected $hidden = [
    ];
	protected function trip()
	{
		return $this->belongsTo('App\Trip','TripId','ID');
	}	

}