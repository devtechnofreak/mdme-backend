<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'Drivers';
	public $timestamps = false;
	protected $with = array('deviceinfo'); 
	protected $appends = array('file_url','driver_id');
    protected $fillable = [
        'DCode','fkUsername','LastName','FirstName','MI','Username','Password','Address','PhoneNum','AlternateNum','HireDate','FireDate','Active','Picture','Comments','DOB','DME_DCode','fkPictureFileID','EmailAddress'
    ];
	protected $primaryKey ='ID';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'Password',
    ];
	
	public function deviceinfo()
	{
		return $this->hasOne('App\DeviceInfo','Driver_ID','ID');
	}
	
	public function vehiclelist()
	{
		return Vehicle::where('fkUsername',$this->fkUsername)->get();
	}
	protected function profile()
	{
		return $this->belongsTo('App\File','fkPictureFileID','FileID');
	}
	
	protected function dispatcher()
	{
		return $this->belongsTo('App\Dispatcher','fkUsername','Username');
	}
	
	public function getFileUrlAttribute()
	{
		if($this->profile)
			return	config('constant.app.url').'Images/'.$this->profile->FileName;
		else
			return "";
	}
	public function getDriverIdAttribute()
	{
		return (string)$this->ID;
	}
}
