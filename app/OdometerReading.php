<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class OdometerReading extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'OdometerReadings';
	public $timestamps = false;
	protected $primaryKey ='ID';
	protected $appends = array('punch_id');
	
    protected $fillable = array(
       'fkDriverID','fkUserName','fkVehicleID','TripDate','StartTime','EndTime','StartReading','EndReading','Date','Timezone','enddate'
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
    */
    protected $hidden = [
    ];
	
	public function getPunchIdAttribute()
	{
		return (string)$this->ID;
	}
}