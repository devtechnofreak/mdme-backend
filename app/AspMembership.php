<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class AspMembership extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'aspnet_Membership';

	protected $connection ='user';
	public $timestamps = false;
	protected $primaryKey ='UserId';
	
    protected $fillable = array(
    	'ApplicationId','UserId','Password','PasswordFormat','PasswordSalt','MobilePIN','Email','LoweredEmail','PasswordQuestion','PasswordAnswer','IsApproved','IsLockedOut','CreateDate','LastLoginDate','LastPasswordChangedDate',
    	'LastLockoutDate','FailedPasswordAttemptCount','FailedPasswordAttemptWindowStart','FailedPasswordAnswerAttemptCount','FailedPasswordAnswerAttemptWindowStart','Comment'
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
    */
    protected $hidden = [
    	'MobilePIN','Email','LoweredEmail','PasswordQuestion','PasswordAnswer','IsApproved','IsLockedOut','CreateDate','LastLoginDate','LastPasswordChangedDate',
    	'LastLockoutDate','FailedPasswordAttemptCount','FailedPasswordAttemptWindowStart','FailedPasswordAnswerAttemptCount','FailedPasswordAnswerAttemptWindowStart','Comment'
    ];
	

}