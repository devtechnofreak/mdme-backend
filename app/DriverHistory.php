<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class DriverHistory extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'DriverChangeshistory';
	public $timestamps = false;
	protected $primaryKey ='ID';
	
    protected $fillable = array(
        'TripID','Drvrfrom','DrvrTo','CreatedDate'
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
    */
    protected $hidden = [
    ];
	

}