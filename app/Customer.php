<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'Customers';
	public $timestamps = false;
	protected $primaryKey ='ID';
	
	
    protected $fillable = array(
    'fkUsername','LastName','FirstName','MI','Medicaid','MedicaidNum','County','DOB','Address','PhoneNum','BillingName','BillingPhoneNum','BillingAddress','Comments','Active','DME_ID','ReimbursementID'
    ,'County2','CustomerID','InsertDate'
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
    */
    protected $hidden = [
    ];
	
	public function getCustomerNameAttribute()
	{
		return $this->LastName.', '.$this->FirstName;
	}
	
}