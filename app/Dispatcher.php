<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Dispatcher extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'Users';
	public $timestamps = false;
	protected $primaryKey ='';
	
    protected $fillable = array(
        'Username','Email','FirstName','LastName','Active','Demo'
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
    */
    protected $hidden = [
    ];
	
	public function timezone()
	{
		return $this->hasOne('App\TimeZone','UserId','Username');
	}
}