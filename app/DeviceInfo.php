<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class DeviceInfo extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'Device_Info';
	public $timestamps = false;
	protected $primaryKey ='ID';
	//protected $with = array('notes'); 
	
    protected $fillable = array(
        'Driver_ID','Device_Token','Access_Token','Device_Name','Device_Type','App_Version','Date_Created'
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array();
	
    public function user()
    {
        return $this->belongsTo('App\User','Driver_ID');
    }	

}